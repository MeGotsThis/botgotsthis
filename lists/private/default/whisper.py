from source.data import WhisperCommand
from typing import Callable, Mapping, Optional

commands = {}  # type: Mapping[str, Optional[WhisperCommand]]
