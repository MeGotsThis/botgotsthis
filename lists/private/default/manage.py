from source.data import ManageBotCommand
from typing import Callable, Mapping, Optional

methods = {}  # type: Mapping[str, ManageBotCommand]
