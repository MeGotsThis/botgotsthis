﻿from typing import Mapping, Optional

features = {
    'textconvert': None,
    'modpyramid': None,
    'modwall': None,
    'nocustom': None,
    'nourlredirect': None,
    }  # type: Mapping[str, Optional[str]]
